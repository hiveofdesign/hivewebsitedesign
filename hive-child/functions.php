<?php
// pull version number from the file description in the top of the file
$theme_data = wp_get_theme();
define( 'THEME_VERSION', $theme_data->Version );

function enqueue_styles() {
	
	// enqueue parent styles
    wp_enqueue_style('fontawesome', get_template_directory_uri() .'/css/fontawesome-all.min.css');
    wp_enqueue_style('bootstrap', get_template_directory_uri() .'/css/bootstrap.min.css');
    wp_enqueue_style('animate', get_template_directory_uri() .'/css/animate.css');
	wp_enqueue_style('hive-style', get_template_directory_uri() .'/style.css', array(), '2');

    // enqueue child styles
	wp_enqueue_style('hive-child', get_stylesheet_directory_uri() .'/style.css', array('hive-style'), THEME_VERSION, 'all');
	
}
add_action('wp_enqueue_scripts', 'enqueue_styles');


// Add support for custom color scheme.
		add_theme_support( 'editor-color-palette', array(
			array(
				'name'  => __( 'Primary Colour', 'hive' ),
				'slug'  => 'primary',
				'color' => '#000',
			),
			array(
				'name'  => __( 'Very Light Gray', 'hive' ),
				'slug'  => 'very-light-gray',
				'color' => '#eee',
			),
			array(
				'name'  => __( 'Very Dark Gray', 'hive' ),
				'slug'  => 'very-dark-gray',
				'color' => '#444',
			),
            array(
				'name'  => __( 'White', 'hive' ),
				'slug'  => 'white',
				'color' => '#fff',
			),
		) );

/* Let Editors manage widgets and menus, and run this only once */
function isa_editor_manage_menus() {
    if ( get_option( 'isa_add_cap_editor_once' ) != 'done' ) {
        $edit_editor = get_role('editor'); // Get the user role
        $edit_editor->add_cap('edit_theme_options');
        update_option( 'isa_add_cap_editor_once', 'done' );
    }
}
add_action( 'init', 'isa_editor_manage_menus' );

/* Let Editors manage forms */
// Must use all three filters for this to work properly. 
add_filter( 'ninja_forms_admin_parent_menu_capabilities','nf_subs_capabilities' ); // Parent Menu
add_filter( 'ninja_forms_admin_all_forms_capabilities','nf_subs_capabilities' ); // Forms Submenu
add_filter( 'ninja_forms_admin_submissions_capabilities','nf_subs_capabilities' ); // Submissions Submenu

function nf_subs_capabilities( $cap ) {
    return 'edit_posts'; // EDIT: User Capability
}

?>